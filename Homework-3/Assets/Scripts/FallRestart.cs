﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FallRestart : MonoBehaviour {

	[SerializeField] string m_levelToLoad;
	[SerializeField] float m_secondsBeforeRestart;

	// Use this for initialization
	void OnTriggerEnter (Collider other){

		Invoke ("LoadLevel", m_secondsBeforeRestart);
	}

	void LoadLevel() {

		SceneManager.LoadScene(m_levelToLoad);
	}
}
