﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinRotation : MonoBehaviour {

	//m_eulerAngles in Lesson Slides.
	Vector3 m_rotation;
	[SerializeField] float rotationSpeed = 100f;

	// Use this for initialization
	void Start () {
		m_rotation = gameObject.transform.rotation.eulerAngles;
		
	}
	
	// Update is called once per frame
	void LateUpdate () {


		m_rotation.y = m_rotation.y + rotationSpeed * Time.deltaTime;

		gameObject.transform.rotation = Quaternion.Euler (m_rotation);
	}
	
}
