﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereController : MonoBehaviour {


	[SerializeField] float m_movementSpeed = 100;
	[SerializeField] float m_jumpForce = 100;



	Rigidbody m_sphereRigidbody;

	// Use this for initialization
	void Start () {

		m_sphereRigidbody = gameObject.GetComponent<Rigidbody>();

		if (m_sphereRigidbody == null){

			Debug.Log("No Rigidbody found in Cube Controller");
		}
		
	}
	
	// Update is called once per frame
	void Update () {

	float horizontalMovement = Input.GetAxis ("Horizontal");
	float verticalMovement = Input.GetAxis ("Vertical");

	Vector3 movementForce = new Vector3();

	movementForce.x = horizontalMovement*m_movementSpeed*Time.deltaTime;
	movementForce.z = verticalMovement*m_movementSpeed*Time.deltaTime;

	m_sphereRigidbody.AddForce(movementForce,ForceMode.Impulse);


		if (Input.GetKeyDown (KeyCode.Space)){

			m_sphereRigidbody.AddForce(new Vector3 (0, m_jumpForce, 0), ForceMode.Impulse); 

		}
	}
}
